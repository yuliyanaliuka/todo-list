import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import { Observable, tap } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(
        (event) => {
          // success
          if (event instanceof HttpResponse) {
            this.snackBar.open('Success !', 'Close');
          }
        },
        (error) => {
          // fail
          if (error instanceof HttpErrorResponse) {
            this.snackBar.open('HTTP request failed !', 'Close');
          }
        }
      )
    );
  }
}
