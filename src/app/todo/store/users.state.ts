import { User } from "../types/user.type";

export interface UsersState {
  users: User[],
  lastLoadedAt: Date,
}

export const state: UsersState = {
  users: null,
  lastLoadedAt: null,
};
