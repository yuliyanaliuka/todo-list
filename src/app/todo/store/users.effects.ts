import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { lastLoadedAt, UsersActionTypes } from "./users.actions";
import { map } from "rxjs";

@Injectable()
export class UsersEffects {
  constructor(private actions$: Actions) {
  }

  lastLoadedAt$ = createEffect(() => this.actions$.pipe(
      ofType(UsersActionTypes.USERS_LOADED_SUCCESS),
      map(() => {
        return lastLoadedAt({ lastLoadedAt: new Date() })
      }),
    )
  );
}
