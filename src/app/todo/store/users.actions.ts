import { createAction, props } from "@ngrx/store";
import { User } from "../types/user.type";

export enum UsersActionTypes {
  LOAD_USERS = '[USER LIST] Load users',
  USERS_LOADED_SUCCESS = '[USER LIST] Users loaded success',
  USERS_LOADED_FAIL = '[USER LIST] Users loaded fail',
  LAST_LOADED_AT = '[USER LIST] Last loaded at'
}

export const loadUsers = createAction(UsersActionTypes.LOAD_USERS);
export const usersLoadedSuccess = createAction(
  UsersActionTypes.USERS_LOADED_SUCCESS,
  props<{ users: User[] }>()
);
export const usersLoadedFail = createAction(UsersActionTypes.USERS_LOADED_FAIL);

export const lastLoadedAt = createAction(
  UsersActionTypes.LAST_LOADED_AT,
  props<{ lastLoadedAt: Date }>()
);
