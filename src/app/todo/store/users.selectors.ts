import { createFeatureSelector, createSelector } from "@ngrx/store";
import { UsersState } from "./users.state";

export const featureSelector = createFeatureSelector<UsersState>('users');

export const usersSelector = createSelector(
  featureSelector,
  (state: UsersState) => state.users
);

export const lastLoadedAtSelector = createSelector(
  featureSelector,
  (state: UsersState) => state.lastLoadedAt
);
