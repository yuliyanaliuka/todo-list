import { createReducer, on } from "@ngrx/store";
import { UsersState } from "./users.state";
import { lastLoadedAt, usersLoadedSuccess } from "./users.actions";

const initialState: UsersState = {
  users: null,
  lastLoadedAt: null,
};
export const usersReducer = createReducer(
  initialState,
  on(usersLoadedSuccess, (state, { users }) => ({ ...state, users })),
  on(lastLoadedAt, (state, { lastLoadedAt }) => ({ ...state, lastLoadedAt })),
);
