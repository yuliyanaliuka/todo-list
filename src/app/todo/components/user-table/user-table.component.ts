import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UserService } from "../../services/user.service";
import { Subject, takeUntil, tap } from "rxjs";
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { usersLoadedSuccess } from "../../store/users.actions";
import { Store } from "@ngrx/store";
import { lastLoadedAtSelector, usersSelector } from "../../store/users.selectors";

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public users: any[] = [];
  public columns: string[] = ['firstName', 'lastName', 'email', 'company'];
  public dataSource: any;
  public total: number;
  public pageSizeOptions: number[] = [5, 10, 20];
  public pageSize: number = this.pageSizeOptions[0];
  public page: number = 0;
  public lastLoadedAt$ = this.store.select(lastLoadedAtSelector);


  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private userService: UserService,
              private store: Store) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.loadUsers(true);
  }

  loadNewPage(event: PageEvent) {
    this.page = event.pageIndex;
    this.pageSize = event.pageSize;

    this.loadUsers();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private loadUsers(initSortPage: boolean = false) {
    const usersState$ = this.store.select(usersSelector);
    usersState$.pipe(
      tap(value => {
        if (!value) {
          this.getUsers(initSortPage);
        }
      })
    )
      .subscribe(users => {
        this.users = users;
        this.dataSource = new MatTableDataSource<any>(this.users);

      })

  }

  private getUsers(initSortPage: boolean = false) {
    const params = {
      skip: this.pageSize * this.page,
      limit: this.pageSize,
    }

    this.userService.getAllUsers(params)
      .pipe(takeUntil(this.destroy$))
      .subscribe(userResponse => {
        this.store.dispatch(usersLoadedSuccess({ users: userResponse.users }));

        if (!this.total) {
          this.total = userResponse.total;
        }
      });
  }

  private initDataSource() {
    this.dataSource = new MatTableDataSource<any>(this.users);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource.sortingDataAccessor = (user: any, property: string) => {
      if (property === 'company') {
        return user.company.name;
      } else {
        return user[property];
      }
    };
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
