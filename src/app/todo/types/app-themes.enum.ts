export enum AppThemes {
  Light = 'light-theme',
  Dark = 'dark-theme',
}
