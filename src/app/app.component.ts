import { Component, OnInit } from '@angular/core';
import { AppThemes } from "./todo/types/app-themes.enum";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  theme: string;

  ngOnInit() {
    const theme = localStorage.getItem('theme');
    this.theme = theme || AppThemes.Light;
  }

}
