import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from "../../../services/auth.service";
import { AppThemes } from "../../../todo/types/app-themes.enum";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() theme: string;
  @Output() themeChange: EventEmitter<string> = new EventEmitter<string>();

  constructor(private authService: AuthService) {
  }

  logout(): void {
    this.authService.logout();
  }

  toggleTheme() {
    this.theme = this.theme === AppThemes.Dark ? AppThemes.Light : AppThemes.Dark;
    this.themeChange.emit(this.theme);
    localStorage.setItem('theme', this.theme);
  }
}
